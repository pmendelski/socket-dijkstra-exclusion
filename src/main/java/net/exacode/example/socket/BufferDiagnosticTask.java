package net.exacode.example.socket;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimerTask;

import net.exacode.example.socket.timer.MultiTimer;
import net.exacode.example.socket.timer.MultiTimerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class BufferDiagnosticTask extends TimerTask {
	private final State state;

	private final String fileName;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private static final String COLUMN_SEPARATOR = "\t";

	private static final String ROW_SEPARATOR = "\n";

	private static final String SUMMARY_LABEL = "SUMMARY";

	public BufferDiagnosticTask(State state, String fileName) {
		super();
		this.state = state;
		this.fileName = fileName;
	}

	private NodeStatus getBufferStatus() throws InterruptedException {
		LocalStatus bufferStatus = state.getLocalStatus();
		NodeStatus bufferNodeStatus = new NodeStatus();
		bufferNodeStatus.setNodeId(state.getNodeDescriptor().getId());
		bufferNodeStatus.setAverageCommunicationTime(bufferStatus
				.getCommunicationAvgTime());
		bufferNodeStatus
				.setAverageProcessTime(bufferStatus.getProcessAvgTime());
		return bufferNodeStatus;
	}

	private StringBuilder generateTable(Collection<NodeStatus> nodeStatuses) {
		StringBuilder builder = new StringBuilder();
		builder.append("node_id").append(COLUMN_SEPARATOR);
		builder.append("avg_production_time").append(COLUMN_SEPARATOR);
		builder.append("avg_consumption_time").append(COLUMN_SEPARATOR);
		builder.append("avg_communication_time").append(COLUMN_SEPARATOR);
		builder.append("avg_process_time").append(ROW_SEPARATOR);
		for (NodeStatus nodeStatus : nodeStatuses) {
			builder.append(nodeStatus.getNodeId()).append(COLUMN_SEPARATOR);
			builder.append(nodeStatus.getAverageProductionTime()).append(
					COLUMN_SEPARATOR);
			builder.append(nodeStatus.getAverageConsumptionTime()).append(
					COLUMN_SEPARATOR);
			builder.append(nodeStatus.getAverageCommunicationTime()).append(
					COLUMN_SEPARATOR);
			builder.append(nodeStatus.getAverageProcessTime()).append(
					COLUMN_SEPARATOR);
			builder.append(ROW_SEPARATOR);
		}
		return builder;
	}

	private StringBuilder generateSummary(Collection<NodeStatus> nodeStatuses) {
		MultiTimer processTimer = new MultiTimerImpl();
		MultiTimer communicationTimer = new MultiTimerImpl();
		MultiTimer consumptionTimer = new MultiTimerImpl();
		MultiTimer productionTimer = new MultiTimerImpl();
		for (NodeStatus status : nodeStatuses) {
			communicationTimer.addResultDenyZero(status
					.getAverageCommunicationTime());
			processTimer.addResultDenyZero(status.getAverageProcessTime());
			consumptionTimer.addResultDenyZero(status
					.getAverageConsumptionTime());
			productionTimer
					.addResultDenyZero(status.getAverageProductionTime());
		}
		StringBuilder builder = new StringBuilder();
		builder.append(ROW_SEPARATOR).append(ROW_SEPARATOR)
				.append(SUMMARY_LABEL).append(ROW_SEPARATOR);

		builder.append("avg_production_time").append(COLUMN_SEPARATOR)
				.append(productionTimer.getAverage()).append(ROW_SEPARATOR);
		builder.append("avg_consumption_time").append(COLUMN_SEPARATOR)
				.append(consumptionTimer.getAverage()).append(ROW_SEPARATOR);
		builder.append("avg_communication_time").append(COLUMN_SEPARATOR)
				.append(communicationTimer.getAverage()).append(ROW_SEPARATOR);
		builder.append("avg_process_time").append(COLUMN_SEPARATOR)
				.append(processTimer.getAverage()).append(ROW_SEPARATOR);

		return builder;
	}

	@Override
	public void run() {
		List<NodeStatus> statuses = state.getSystemStatus().getNodeStatuses();
		try {
			statuses.add(getBufferStatus());
			Collections.sort(statuses, new Comparator<NodeStatus>() {

				@Override
				public int compare(NodeStatus o1, NodeStatus o2) {
					long delta = o1.getAverageConsumptionTime()
							- o2.getAverageConsumptionTime();
					return (delta > 0) ? 1 : -1;
				}

			});

			Collections.sort(statuses, new Comparator<NodeStatus>() {

				@Override
				public int compare(NodeStatus o1, NodeStatus o2) {
					long delta = o1.getAverageProductionTime()
							- o2.getAverageProductionTime();
					return (delta > 0) ? 1 : -1;
				}

			});
			StringBuilder builder = generateTable(statuses);
			builder.append(generateSummary(statuses));
			write(builder);
		} catch (InterruptedException e) {
			logger.error("Could not get local node descriptor", e);
		}
	}

	private void write(StringBuilder builder) {
		try {
			Path file = Paths.get(fileName);
			Files.deleteIfExists(file);
			file = Files.createFile(file);
			try (BufferedWriter writer = Files.newBufferedWriter(file,
					StandardCharsets.UTF_8)) {
				writer.write(builder.toString());
			}
		} catch (IOException e) {
			logger.error("Could not write results to file.", e);
		}
	}
}