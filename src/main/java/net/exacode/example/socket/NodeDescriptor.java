package net.exacode.example.socket;

import java.net.Socket;

/**
 * Describes each node.
 * 
 * @author pmendelski
 * 
 */
public class NodeDescriptor {
	private final String hostAddress;
	private final int port;
	private final String id;

	private Socket socket;

	public NodeDescriptor(String hostname, int port) {
		super();
		if (hostname.equalsIgnoreCase("localhost")) {
			hostname = "127.0.0.1";
		}
		this.hostAddress = hostname;
		this.port = port;
		this.id = hostname + ":" + port;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public int getPort() {
		return port;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("NodeDescriptor [id=").append(id).append("]");
		return builder.toString();
	}

	public Socket getSocket() {
		return socket;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NodeDescriptor other = (NodeDescriptor) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
