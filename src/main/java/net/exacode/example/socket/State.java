package net.exacode.example.socket;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents a state of a node.
 * 
 * @author pmendelski
 * 
 */
public class State {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SystemStatus systemStatus = new SystemStatus();

	private final SystemRole systemRole;

	private boolean firstPhase = false;

	private boolean secondPhase = false;

	private String priorityNode;

	private int localServerPort;

	private int stock = 0;

	private final LocalStatus localStatus = new LocalStatus();

	private NodeDescriptor nodeDescriptor;

	private List<NodeDescriptor> otherNodes;

	private NodeDescriptor bufferNode;

	public State(SystemRole systemRole, int port) {
		super();
		this.systemRole = systemRole;
		this.localServerPort = port;
	}

	public boolean isFirstPhase() {
		return firstPhase;
	}

	public void setFirstPhase(boolean firstPhase) {
		this.firstPhase = firstPhase;
	}

	public boolean isSecondPhase() {
		return secondPhase;
	}

	public void setSecondPhase(boolean secondPhase) {
		this.secondPhase = secondPhase;
	}

	public SystemRole getSystemRole() {
		return systemRole;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getPriorityNode() {
		return priorityNode;
	}

	public synchronized void setPriorityNode(String priorityNode) {
		this.priorityNode = priorityNode;
	}

	public synchronized NodeDescriptor getNodeDescriptor()
			throws InterruptedException {
		if (nodeDescriptor == null) {
			logger.trace("Waiting for node descriptor...");
			this.wait();
		}
		return nodeDescriptor;
	}

	public synchronized void setNodeDescriptor(NodeDescriptor nodeDescriptor) {
		logger.trace("Setting node descriptor: {}", nodeDescriptor);
		this.nodeDescriptor = nodeDescriptor;
		this.notifyAll();
	}

	public List<NodeDescriptor> getOtherNodes() {
		return otherNodes;
	}

	public void setOtherNodes(List<NodeDescriptor> otherNodes) {
		this.otherNodes = otherNodes;
	}

	public NodeDescriptor getBufferNode() {
		return bufferNode;
	}

	public void setBufferNode(NodeDescriptor bufferNode) {
		this.bufferNode = bufferNode;
	}

	public int getLocalServerPort() {
		return localServerPort;
	}

	public void setLocalServerPort(int localServerPort) {
		this.localServerPort = localServerPort;
	}

	public SystemStatus getSystemStatus() {
		return systemStatus;
	}

	public LocalStatus getLocalStatus() {
		return localStatus;
	}

}