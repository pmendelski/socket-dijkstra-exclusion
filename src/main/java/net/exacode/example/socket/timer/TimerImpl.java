package net.exacode.example.socket.timer;

/**
 * Constructed to make time records. Uses System.currentTimeMillis().
 * 
 * @author pmendelski
 */
public class TimerImpl implements Timer {

	private Long startTime = null;

	private Long result = null;

	/**
	 * Starts the timer.
	 */
	@Override
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Stops the timer.
	 */
	@Override
	public void stop() {
		if (startTime == null) {
			throw new IllegalArgumentException(
					"Stop method invoked before start method");
		}
		result = System.currentTimeMillis() - startTime;
	}

	/**
	 * Returns result as value in milliseconds.
	 * 
	 * @return
	 */
	@Override
	public long getResult() {
		if (result == null) {
			throw new IllegalArgumentException(
					"Get results invoked before stop method");
		}
		return result;
	}
}
