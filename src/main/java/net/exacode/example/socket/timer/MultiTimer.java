package net.exacode.example.socket.timer;

/**
 * Stores timer results from many timers.
 * 
 * @author pmendelski
 * 
 */
public interface MultiTimer {

	void addResult(long result);

	void addResultDenyZero(long result);

	long getAverage();

	long getMax();

	long getMin();

	int getResultCount();

	long getResultSum();

}
