package net.exacode.example.socket.timer;


import java.text.DecimalFormat;

/**
 * Prints formatted timer results adds time units.
 * 
 * @author pmendelski
 */
public class TimeFormatterImpl implements TimeFormatter {

	/**
	 * Returns formatted timer result.
	 * 
	 * @return
	 */
	public String format(long miliseconds) {
		double sec = miliseconds / 1000d;
		long min = Math.round(Math.floor(sec / 60));
		long hour = Math.round(Math.floor(min / 60));
		String secFormatted = (new DecimalFormat("##.####")).format(sec);
		String result = null;
		if (sec < 1) {
			result = miliseconds + "[ms]";
		} else if (min < 1) {
			result = secFormatted + "[s.ms]";
		} else if (hour < 1) {
			result = min + ":" + secFormatted + "[m:s.ms]";
		} else {
			result = hour + ":" + min + ":" + secFormatted + "[h:m:s.ms]";
		}
		return result;
	}
}
