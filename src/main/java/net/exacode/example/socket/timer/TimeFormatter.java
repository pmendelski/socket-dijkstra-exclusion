package net.exacode.example.socket.timer;

/**
 * Fomrats time passed in milliseconds.
 * @author pmendelski
 *
 */
public interface TimeFormatter {

	/**
	 * Prints formatted timer results. 
	 * Example output: 150[ms].
	 * @param timer
	 * @return
	 */
	String format(long miliseconds);
	
}
