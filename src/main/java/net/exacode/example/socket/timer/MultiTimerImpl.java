package net.exacode.example.socket.timer;

/**
 * Simple MultiTimer implementation.
 * 
 * @author pmendelski
 * 
 */
public class MultiTimerImpl implements MultiTimer {

	private long max = Long.MIN_VALUE;

	private long min = Long.MAX_VALUE;

	private int resultCount = 0;

	private long resultSum = 0;

	@Override
	public void addResult(long result) {
		resultSum += result;
		resultCount++;
		if (max < result) {
			max = result;
		}
		if (min > result) {
			min = result;
		}
	}

	@Override
	public long getAverage() {
		if (resultCount == 0) {
			return 0;
		}
		return resultSum / resultCount;
	}

	@Override
	public long getMax() {
		return max;
	}

	@Override
	public long getMin() {
		return min;
	}

	@Override
	public int getResultCount() {
		return resultCount;
	}

	@Override
	public long getResultSum() {
		return resultSum;
	}

	@Override
	public void addResultDenyZero(long result) {
		if (result != 0) {
			addResult(result);
		}
	}

}
