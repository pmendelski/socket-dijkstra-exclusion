package net.exacode.example.socket.timer;

/**
 * Timer counts the time of instruction invokations.
 * @author pmendelski
 *
 */
public interface Timer {
	/**
	 * Starts or restarts the timer.
	 */
	void start();

	/**
	 * Stops the timer.
	 */
	void stop();

	/**
	 * Returns result as value in milliseconds.
	 * 
	 * @return
	 */
	long getResult();
}
