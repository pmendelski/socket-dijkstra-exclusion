package net.exacode.example.socket;

/**
 * Describes available values in system.
 * 
 * @author pmendelski
 * 
 */
public enum ValueTypes {
	/**
	 * True/false value. Describes if a node wants to get access to critical
	 * section.
	 */
	FIRST_PHASE,
	/**
	 * True/false value. Describes if a node is second phase of getting access
	 * to critical section.
	 */
	SECOND_PHASE,
	/**
	 * Id of a node with priority.
	 */
	PRIORITY_NODE,
	/**
	 * Value kept in critical section.
	 */
	STOCK,

	/**
	 * Average time of production
	 */
	AVG_PRODUCTION_TIME,

	/**
	 * Average time of consumption
	 */
	AVG_CONSUMPTION_TIME,

	/**
	 * Average time of data process
	 */
	AVG_PROCESS_TIME,

	/**
	 * Average time of communication
	 */
	AVG_COMMUNICATION_TIME
}
