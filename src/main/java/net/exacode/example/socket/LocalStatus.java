package net.exacode.example.socket;

import net.exacode.example.socket.timer.MultiTimer;
import net.exacode.example.socket.timer.MultiTimerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalStatus {
	private final MultiTimer processTimer = new MultiTimerImpl();

	private final MultiTimer communicationTimer = new MultiTimerImpl();

	private final MultiTimer operationTimer = new MultiTimerImpl();

	private final Logger loggger = LoggerFactory.getLogger(getClass());

	public void addOperationTime(long time) {
		synchronized (operationTimer) {
			loggger.trace("Adding operation time: {}", time);
			operationTimer.addResult(time);
		}
	}

	public void addCommunicationTime(long time) {
		synchronized (communicationTimer) {
			communicationTimer.addResult(time);
		}
	}

	public void addProcessTime(long time) {
		synchronized (processTimer) {
			processTimer.addResult(time);
		}
	}

	public long getCommunicationAvgTime() {
		return communicationTimer.getAverage();
	}

	public long getProcessAvgTime() {
		return processTimer.getAverage();
	}

	public long getOperationAvgTime() {
		return operationTimer.getAverage();
	}
}
