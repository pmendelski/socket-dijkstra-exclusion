package net.exacode.example.socket;

import net.exacode.example.socket.logic.ConsumerCriticalSectionListener;
import net.exacode.example.socket.logic.CriticalSectionListener;
import net.exacode.example.socket.logic.ProducerCriticalSectionListener;

/**
 * Described node'e role in the system.
 * 
 * @author pmendelski
 * 
 */
public enum SystemRole {
	/**
	 * Given node is consumer
	 */
	CONSUMER {
		@Override
		public CriticalSectionListener getCriticalSectionListener() {
			return new ConsumerCriticalSectionListener();
		}
	},
	/**
	 * Given node is producer
	 */
	PRODUCER {
		@Override
		public CriticalSectionListener getCriticalSectionListener() {
			return new ProducerCriticalSectionListener();
		}
	},
	/**
	 * Given node is buffer
	 */
	BUFFER {
		@Override
		public CriticalSectionListener getCriticalSectionListener() {
			return null;
		}
	};

	public abstract CriticalSectionListener getCriticalSectionListener();

}
