package net.exacode.example.socket;

public class NodeStatus {
	private String nodeId;
	private long averageProcessTime;
	private long averageCommunicationTime;
	private long averageProductionTime;
	private long averageConsumptionTime;

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public long getAverageProcessTime() {
		return averageProcessTime;
	}

	public void setAverageProcessTime(long averageProcessTime) {
		this.averageProcessTime = averageProcessTime;
	}

	public long getAverageCommunicationTime() {
		return averageCommunicationTime;
	}

	public void setAverageCommunicationTime(long averageCommunicationTime) {
		this.averageCommunicationTime = averageCommunicationTime;
	}

	public long getAverageProductionTime() {
		return averageProductionTime;
	}

	public void setAverageProductionTime(long averageProductionTime) {
		this.averageProductionTime = averageProductionTime;
	}

	public long getAverageConsumptionTime() {
		return averageConsumptionTime;
	}

	public void setAverageConsumptionTime(long averageConsumptionTime) {
		this.averageConsumptionTime = averageConsumptionTime;
	}

}
