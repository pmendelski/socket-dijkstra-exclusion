package net.exacode.example.socket;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import net.exacode.example.socket.logic.CriticalSectionListener;
import net.exacode.example.socket.logic.SocketDijkstraExclusionTemplate;
import net.exacode.example.socket.server.ServiceBroadcaster;
import net.exacode.example.socket.server.ServiceServer;
import net.exacode.example.socket.server.ServiceServerImpl;
import net.exacode.example.socket.service.ServiceFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Starting point for this program.
 * 
 * @author pmendelski
 * 
 */
public class EntryPoint {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(EntryPoint.class.getClass());

	private static final String DEFAULT_DIAGNOSTIC_FILE = "diagnostic.txt";

	private static final int DEFAULT_DIAGNOSTIC_DELTA = 10000;

	protected EntryPoint() {

	}

	public static void main(String[] args) throws IOException,
			NumberFormatException, InterruptedException {
		int inputPort = 0;
		List<NodeDescriptor> customerProducerNodes = null;
		NodeDescriptor bufferNode = null;
		SystemRole role = SystemRole.CONSUMER;
		String diagnosticFileName = DEFAULT_DIAGNOSTIC_FILE;
		int slowMode = 0;
		int diagnosticDelta = DEFAULT_DIAGNOSTIC_DELTA;
		for (int i = 0; i + 1 < args.length; ++i) {
			if (args[i].equals("-p")) {
				LOGGER.info("Found -p (input port) argument: " + args[i + 1]);
				inputPort = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equals("-cp")) {
				LOGGER.info("Found -cp (consumer/producer addresses) argument: "
						+ args[i + 1]);
				customerProducerNodes = splitSocketAddresses(args[i + 1]);
			}
			if (args[i].equals("-b")) {
				LOGGER.info("Found -b (buffer addresses) argument: "
						+ args[i + 1]);
				String[] addr = args[i + 1].split(":");
				bufferNode = new NodeDescriptor(addr[0],
						Integer.parseInt(addr[1]));
			}
			if (args[i].equals("-log")) {
				LOGGER.info("Found -log argument: " + args[i + 1]);
				new LoggerConfigLoader()
						.loadLoggerConfig(new File(args[i + 1]));
			}
			if (args[i].equals("-r")) {
				LOGGER.info("Found -r (system role: CONSUMER | PRODUCER | BUFFER) argument: "
						+ args[i + 1]);
				role = SystemRole.valueOf(args[i + 1].toUpperCase());
			}
			if (args[i].equals("-slow")) {
				LOGGER.info("Found -slow (slow mode) argument: " + args[i + 1]);
				slowMode = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equals("-diagnostic")) {
				LOGGER.info("Found -diagnostic (diagnostic delta) argument: "
						+ args[i + 1]);
				diagnosticDelta = Integer.parseInt(args[i + 1]);
			}
			if (args[i].equals("-diagnosticFile")) {
				LOGGER.info("Found -diagnosticFile (diagnostic file) argument: "
						+ args[i + 1]);
				diagnosticFileName = args[i + 1];
			}
		}
		if (inputPort == 0 || !SystemRole.BUFFER.equals(role)
				&& (customerProducerNodes == null || bufferNode == null)) {
			throw new IllegalArgumentException(
					"No input or/and output address given");
		}

		State state = new State(role, inputPort);
		if (SystemRole.BUFFER.equals(role)) {
			state.setNodeDescriptor(new NodeDescriptor("127.0.0.1", inputPort));
		}
		state.setBufferNode(bufferNode);
		state.setOtherNodes(customerProducerNodes);
		startSocketServer(state, slowMode);
		startNodeLogic(state, diagnosticDelta, diagnosticFileName);
	}

	private static List<NodeDescriptor> splitSocketAddresses(String text)
			throws UnknownHostException {
		String[] values = text.trim().split("\b*,\b*");

		List<NodeDescriptor> outputSocketAddresses = new ArrayList<NodeDescriptor>();
		for (String outputAddress : values) {
			String[] addr = outputAddress.split(":");
			NodeDescriptor node = new NodeDescriptor(addr[0],
					Integer.parseInt(addr[1]));
			outputSocketAddresses.add(node);
		}

		return outputSocketAddresses;
	}

	private static void startSocketServer(State state, int slowMode)
			throws IOException {
		final ServiceServer socketServer = new ServiceServerImpl(state,
				new ServiceFactory(state, slowMode));
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					socketServer.start();
				} catch (IOException e) {
					LOGGER.error("Could not start server", e);
				}
			}
		}).start();

	}

	private static void startNodeLogic(State state, int diagnosticDelta,
			String diagnosticFile) throws IOException, NumberFormatException,
			InterruptedException {
		if (!SystemRole.BUFFER.equals(state.getSystemRole())) {
			ServiceBroadcaster socketBroadcaster = new ServiceBroadcaster(state);
			LOGGER.debug("Strating broadcaster");
			socketBroadcaster.start();
			LOGGER.debug("Broadcaster started");
			CriticalSectionListener criticalSectionListener = state
					.getSystemRole().getCriticalSectionListener();
			SocketDijkstraExclusionTemplate algorithm = new SocketDijkstraExclusionTemplate(
					state, socketBroadcaster, criticalSectionListener);
			startNodeDiagnostic(socketBroadcaster, diagnosticDelta);
			LOGGER.debug("Diagnostics started");
			algorithm.start();
		} else {
			startBufferDiagnostic(state, diagnosticDelta, diagnosticFile);
		}
	}

	private static void startNodeDiagnostic(
			final ServiceBroadcaster socketBroadcaster,
			final int diagnosticDelta) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						socketBroadcaster.sendStatusData();
						Thread.sleep(diagnosticDelta);
					} catch (InterruptedException e) {
						LOGGER.warn("Diagnostic loop", e);
					}
				}
			}
		}).start();
	}

	private static void startBufferDiagnostic(State state, int diagnosticDelta,
			String fileName) {
		Timer statusTimer = new Timer(true);
		statusTimer.scheduleAtFixedRate(new BufferDiagnosticTask(state,
				fileName), 0, diagnosticDelta);
	}

}
