package net.exacode.example.socket;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * Responsible for changing logging configuration in runtime.
 * 
 * @author pmendelski
 * 
 */
public class LoggerConfigLoader {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void loadLoggerConfig(File file) {
		LoggerContext context = (LoggerContext) LoggerFactory
				.getILoggerFactory();
		try {
			JoranConfigurator configurator = new JoranConfigurator();
			context.reset();
			configurator.setContext(context);
			configurator.doConfigure(file);
		} catch (JoranException je) {
			logger.error("Could not chage logger configuration", je);
		} catch (Exception ex) {
			logger.error("Could not chage logger configuration", ex);
		}
		StatusPrinter.printInCaseOfErrorsOrWarnings(context);
	}
}
