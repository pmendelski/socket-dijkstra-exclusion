package net.exacode.example.socket.service;

import net.exacode.example.socket.State;
import net.exacode.example.socket.service.state.StateResponderService;

/**
 * Produces service objects.
 * 
 * @author pmendelski
 * 
 */
public class ServiceFactory {
	private final State state;

	private final int slowMode;

	public ServiceFactory(State state, int slowMode) {
		super();
		this.state = state;
		this.slowMode = slowMode;
	}

	public Service getSocketService() {
		return new StateResponderService(state, slowMode);
	}
}
