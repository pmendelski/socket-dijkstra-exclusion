package net.exacode.example.socket.service.state;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.exacode.example.socket.State;
import net.exacode.example.socket.server.protocole.DataFrame;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.service.Service;
import net.exacode.example.socket.service.state.command.CommunicationTimeWriteRequestCommand;
import net.exacode.example.socket.service.state.command.ConsumptionTimeWriteRequestCommand;
import net.exacode.example.socket.service.state.command.FirstPhaseRequestCommand;
import net.exacode.example.socket.service.state.command.PriorityNodeRequestCommand;
import net.exacode.example.socket.service.state.command.PriorityNodeWriteRequestCommand;
import net.exacode.example.socket.service.state.command.ProcessTimeWriteRequestCommand;
import net.exacode.example.socket.service.state.command.ProductionTimeWriteRequestCommand;
import net.exacode.example.socket.service.state.command.RequestCommand;
import net.exacode.example.socket.service.state.command.SecondPhaseRequestCommand;
import net.exacode.example.socket.service.state.command.StockRequestCommand;
import net.exacode.example.socket.service.state.command.StockWriteRequestCommand;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service responsible for responding to other nodes' requests.
 * 
 * @author pmendelski
 * 
 */
public class StateResponderService implements Service {

	private static final String HASH_VALUE_SEPARATOR = "#";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final State state;

	private final int slowReposne;

	private final Map<String, RequestCommand> valueRequestResponders = new HashMap<String, RequestCommand>();

	public StateResponderService(State state, int slowResponse) {
		super();
		this.slowReposne = slowResponse;
		this.state = state;
		List<RequestCommand> responders = new ArrayList<RequestCommand>();
		responders.add(new FirstPhaseRequestCommand(state));
		responders.add(new SecondPhaseRequestCommand(state));
		responders.add(new StockRequestCommand(state));
		responders.add(new PriorityNodeRequestCommand(state));
		responders.add(new StockWriteRequestCommand(state));
		responders.add(new PriorityNodeWriteRequestCommand(state));
		responders.add(new ProductionTimeWriteRequestCommand(state));
		responders.add(new ConsumptionTimeWriteRequestCommand(state));
		responders.add(new ProcessTimeWriteRequestCommand(state));
		responders.add(new CommunicationTimeWriteRequestCommand(state));
		for (RequestCommand responder : responders) {
			String triggerHash = generateCommandHash(responder);
			logger.trace("Added request command. Trigger hash: {}", triggerHash);
			valueRequestResponders.put(triggerHash, responder);
		}
	}

	@Override
	public DataFrame process(DataFrame dataModel) throws InterruptedException {
		if (slowReposne > 0) {
			try {
				Thread.sleep(slowReposne);
			} catch (InterruptedException e) {
				logger.warn("Could not slow down response.", e);
			}
		}
		return generateResponse(dataModel);
	}

	private String generateCommandHash(RequestCommand command) {
		StringBuilder builder = new StringBuilder(command.getValueName());
		builder.append(HASH_VALUE_SEPARATOR).append(command.getFramePartType());
		return builder.toString();
	}

	private String generateCommandHash(DataFramePart part) {
		StringBuilder builder = new StringBuilder(part.getValueName());
		builder.append(HASH_VALUE_SEPARATOR).append(part.getType());
		return builder.toString();
	}

	private DataFrame generateResponse(DataFrame dataModel)
			throws InterruptedException {
		DataFrame dataFrame = new DataFrame(state.getNodeDescriptor().getId());
		for (int i = 0; i < dataModel.getPartCount(); ++i) {
			DataFramePart part = dataModel.getPart(i);
			String triggerHash = generateCommandHash(part);
			logger.trace("Searching for command. Trigger hash: {}", triggerHash);
			RequestCommand command = valueRequestResponders.get(triggerHash);
			if (command == null) {
				logger.warn(
						"Command not found. Couldn't response for frame: {}",
						part);
			} else {
				DataFramePart responsePart = command.respond(part,
						dataModel.getId());
				if (responsePart != null) {
					dataFrame.addPart(responsePart);
				}
			}
		}
		if (dataFrame.getPartCount() > 0) {
			return dataFrame;
		} else {
			return null;
		}
	}
}
