package net.exacode.example.socket.service;

import net.exacode.example.socket.server.protocole.DataFrame;

/**
 * Represents an logic that operates on DataModel.
 * 
 * @author pmendelski
 * 
 */
public interface Service {

	DataFrame process(DataFrame dataModel) throws InterruptedException;

}
