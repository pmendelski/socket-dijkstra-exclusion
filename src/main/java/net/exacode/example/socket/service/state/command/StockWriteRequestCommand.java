package net.exacode.example.socket.service.state.command;

import net.exacode.example.socket.State;
import net.exacode.example.socket.ValueTypes;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command that is responsible for changing value of stock variable.
 * 
 * @author pmendelski
 * 
 */
public class StockWriteRequestCommand implements RequestCommand {
	/**
	 * Variable name that triggers this responder
	 */
	public static final String VARIABLE_NAME = ValueTypes.STOCK.toString()
			.toLowerCase();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final State state;

	public StockWriteRequestCommand(State state) {
		super();
		this.state = state;
	}

	@Override
	public DataFramePartType getFramePartType() {
		return DataFramePartType.WRITE;
	}

	@Override
	public DataFramePart respond(DataFramePart part, String nodeId) {
		int newStock = Integer.parseInt(part.getValue());
		logger.debug("Stock value changed from {} to {}", state.getStock(),
				newStock);
		state.setStock(newStock);
		return null;
	}

	@Override
	public String getValueName() {
		return VARIABLE_NAME;
	}
}
