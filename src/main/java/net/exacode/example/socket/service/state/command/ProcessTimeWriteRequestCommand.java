package net.exacode.example.socket.service.state.command;

import net.exacode.example.socket.NodeStatus;
import net.exacode.example.socket.State;
import net.exacode.example.socket.SystemStatus;
import net.exacode.example.socket.ValueTypes;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command that is responsible for changing status data of given node.
 * 
 * @author pmendelski
 * 
 */
public class ProcessTimeWriteRequestCommand implements RequestCommand {
	/**
	 * Variable name that triggers this responder
	 */
	public static final String VARIABLE_NAME = ValueTypes.AVG_PROCESS_TIME
			.toString().toLowerCase();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final State state;

	public ProcessTimeWriteRequestCommand(State state) {
		super();
		this.state = state;
	}

	@Override
	public DataFramePartType getFramePartType() {
		return DataFramePartType.WRITE;
	}

	@Override
	public DataFramePart respond(DataFramePart part, String nodeId) {
		Long newAvgTime = Long.parseLong(part.getValue());
		SystemStatus systemStatus = state.getSystemStatus();
		NodeStatus nodeStatus = systemStatus.getNodeStatus(nodeId);
		logger.trace("Updating avg_process_time value to: {}. Node id: {}",
				state.getStock(), nodeId);
		nodeStatus.setAverageProcessTime(newAvgTime);
		return null;
	}

	@Override
	public String getValueName() {
		return VARIABLE_NAME;
	}
}
