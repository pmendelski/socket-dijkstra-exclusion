package net.exacode.example.socket.service.state.command;

import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

/**
 * Interface for all commands executed by server.
 * 
 * @author pmendelski
 * 
 */
public interface RequestCommand {
	DataFramePart respond(DataFramePart part, String nodeId);

	String getValueName();

	DataFramePartType getFramePartType();
}
