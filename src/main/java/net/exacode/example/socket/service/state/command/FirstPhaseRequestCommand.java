package net.exacode.example.socket.service.state.command;

import net.exacode.example.socket.State;
import net.exacode.example.socket.ValueTypes;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

/**
 * Command that is responsible for responding to other nodes' requests for
 * first_phase value.
 * 
 * @author pmendelski
 * 
 */
public class FirstPhaseRequestCommand implements RequestCommand {
	/**
	 * Variable name that triggers this responder
	 */
	public static final String VARIABLE_NAME = ValueTypes.FIRST_PHASE
			.toString().toLowerCase();

	private final State state;

	public FirstPhaseRequestCommand(State state) {
		super();
		this.state = state;
	}

	@Override
	public String getValueName() {
		return VARIABLE_NAME;
	}

	@Override
	public DataFramePart respond(DataFramePart part, String nodeId) {
		return new DataFramePart(DataFramePartType.RESP, VARIABLE_NAME,
				Boolean.toString(state.isFirstPhase()));
	}

	@Override
	public DataFramePartType getFramePartType() {
		return DataFramePartType.REQ;
	}
}
