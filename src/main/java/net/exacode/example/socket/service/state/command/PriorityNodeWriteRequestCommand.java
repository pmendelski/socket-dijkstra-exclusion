package net.exacode.example.socket.service.state.command;

import net.exacode.example.socket.State;
import net.exacode.example.socket.ValueTypes;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Command that is responsible for changing value of priority_node variable.
 * 
 * @author pmendelski
 * 
 */
public class PriorityNodeWriteRequestCommand implements RequestCommand {
	/**
	 * Variable name that triggers this responder
	 */
	public static final String VARIABLE_NAME = ValueTypes.PRIORITY_NODE
			.toString().toLowerCase();

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final State state;

	public PriorityNodeWriteRequestCommand(State state) {
		super();
		this.state = state;
	}

	@Override
	public DataFramePartType getFramePartType() {
		return DataFramePartType.WRITE;
	}

	@Override
	public DataFramePart respond(DataFramePart part, String nodeId) {
		String newPriorityNodeId = part.getValue();
		logger.trace("Changing priority_node value from {} to {}",
				state.getStock(), newPriorityNodeId);
		state.setPriorityNode(newPriorityNodeId);
		return null;
	}

	@Override
	public String getValueName() {
		return VARIABLE_NAME;
	}
}
