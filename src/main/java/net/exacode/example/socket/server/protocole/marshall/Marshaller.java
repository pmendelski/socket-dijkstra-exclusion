package net.exacode.example.socket.server.protocole.marshall;

import net.exacode.example.socket.server.protocole.DataFrame;

/**
 * Marshalls {@link DataFrame} to text.
 * 
 * @author pmendelski
 * 
 */
public interface Marshaller {

	String marshall(DataFrame serviceData);
}
