package net.exacode.example.socket.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import net.exacode.example.socket.State;
import net.exacode.example.socket.service.ServiceFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Simple implementation of socket service server.
 * 
 * @author pmendelski
 * 
 */
public class ServiceServerImpl implements ServiceServer {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final ServiceFactory serviceFactory;

	private ServerSocket serverSocket;

	private boolean stop = false;

	private final State state;

	public ServiceServerImpl(State state, ServiceFactory serviceFactory) {
		super();
		this.serviceFactory = serviceFactory;
		this.state = state;
	}

	@Override
	public void start() throws IOException {
		stop = false;
		try {
			logger.trace("Creating server socket for port {}",
					state.getLocalServerPort());
			serverSocket = new ServerSocket(state.getLocalServerPort());
			logger.trace("Server socket created");
			while (!stop) {
				Socket socket = serverSocket.accept();
				logger.trace("New connection received.");
				new Thread(new ServiceWrapper(socket,
						serviceFactory.getSocketService(), state)).start();
			}
		} finally {
			close();
		}
	}

	@Override
	public void close() throws IOException {
		stop = true;
		if (serverSocket != null) {
			serverSocket.close();
		}
	}

}
