package net.exacode.example.socket.server.protocole.datapart;

/**
 * Represents a single row of
 * {@link net.exacode.example.socket.server.protocole.DataFrame}
 * 
 * @author pmendelski
 * 
 */
public class DataFramePart {

	private final DataFramePartType type;
	private final String valueName;
	private String value;

	public DataFramePart(DataFramePartType rowType, String valueName) {
		super();
		this.type = rowType;
		this.valueName = valueName;
	}

	public DataFramePart(DataFramePartType rowType, String valueName,
			String value) {
		this(rowType, valueName);
		this.value = value;
	}

	public String getValueName() {
		return valueName;
	}

	public DataFramePartType getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataFramePart [type=").append(type)
				.append(", valueName=").append(valueName).append(", value=")
				.append(value).append("]");
		return builder.toString();
	}

}
