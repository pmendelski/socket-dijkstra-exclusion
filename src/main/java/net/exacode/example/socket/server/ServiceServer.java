package net.exacode.example.socket.server;

import java.io.Closeable;
import java.io.IOException;

/**
 * Responsible for dispatching calls from sockets to given services.
 * 
 * @author pmendelski
 * 
 */
public interface ServiceServer extends Closeable {

	/**
	 * Starts socket server.
	 */
	void start() throws IOException;

}
