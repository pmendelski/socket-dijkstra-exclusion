package net.exacode.example.socket.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import net.exacode.example.socket.State;
import net.exacode.example.socket.server.protocole.DataFrame;
import net.exacode.example.socket.server.protocole.marshall.CsvMarshaller;
import net.exacode.example.socket.server.protocole.marshall.CsvUnmarshaller;
import net.exacode.example.socket.server.protocole.marshall.Marshaller;
import net.exacode.example.socket.server.protocole.marshall.Unmarshaller;
import net.exacode.example.socket.service.Service;
import net.exacode.example.socket.timer.Timer;
import net.exacode.example.socket.timer.TimerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for real service object. Wraps real service object in Runnable
 * interface and mediates communication through sockets.
 * 
 * @author pmendelski
 * 
 */
public class ServiceWrapper implements Runnable {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Socket clientSocket;

	private final Service service;

	private final State state;

	private final Timer processTimer = new TimerImpl();

	private final Timer responseTimer = new TimerImpl();

	private final Unmarshaller frameDataUnmarshaller = new CsvUnmarshaller();

	private final Marshaller frameDataMarshaller = new CsvMarshaller();

	public ServiceWrapper(Socket clientSocket, Service service, State state) {
		super();
		this.clientSocket = clientSocket;
		this.service = service;
		this.state = state;
	}

	@Override
	public void run() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
					true);
			StringBuilder input = new StringBuilder();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.length() == 0) {
					processData(input.toString(), out);
					input = new StringBuilder();
				} else {
					input.append(inputLine);
					input.append("\n");
				}
			}
			logger.trace("Connection closed. Data received:\n{}", input);
			processData(input.toString(), out);
			in.close();
			out.close();
		} catch (IOException | InterruptedException e) {
			logger.error("Error occured during socket service process.", e);
		}
	}

	private void processData(String textData, PrintWriter out)
			throws InterruptedException {

		DataFrame data = frameDataUnmarshaller.unmarshall(textData);
		if (data == null) {
			return;
		}
		logger.trace("Processing received data from {}\n{}",
				clientSocket.getRemoteSocketAddress(), textData);
		Timer processTimer = new TimerImpl();
		processTimer.start();
		DataFrame responseData = service.process(data);
		processTimer.stop();
		state.getLocalStatus().addProcessTime(processTimer.getResult());

		if (responseData != null) {
			sendData(responseData, out);
		}
	}

	private void sendData(DataFrame responseData, PrintWriter out) {
		String responseText = frameDataMarshaller.marshall(responseData);
		logger.trace("Sending service response:\n{}", responseText);
		out.println(responseText);
		out.flush();
		logger.trace("Service response sent");
	}
}
