package net.exacode.example.socket.server.protocole.marshall;

import java.util.Scanner;

import net.exacode.example.socket.server.protocole.DataFrame;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unmarshalles ServiceData from text (title + CSV).
 * 
 * @author pmendelski
 * 
 */
public class CsvUnmarshaller implements Unmarshaller {

	/**
	 * Separates values
	 */
	public static final String VALUE_SEPARATOR = ";";
	/**
	 * Separates lines
	 */
	public static final String LINE_SEPARATOR = "\n";

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public DataFrame unmarshall(String text) {
		DataFrame serviceData = null;
		try (Scanner scanner = new Scanner(text)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine().trim();
				if (line.length() == 0) {
					continue;
				}
				if (serviceData == null) {
					serviceData = new DataFrame(line);
				} else {
					serviceData.addPart(parseDataFramePart(line));
				}
			}
		}
		return serviceData;
	}

	private DataFramePart parseDataFramePart(String line) {
		String[] lineValues = line.trim().split(
				"\\s*" + VALUE_SEPARATOR + "\\s*");
		DataFramePart part = null;
		if (lineValues.length >= 2) {
			DataFramePartType type = DataFramePartType.valueOf(lineValues[0]
					.toUpperCase());
			String valueName = lineValues[1];
			part = new DataFramePart(type, valueName);
			if (lineValues.length > 2) {
				part.setValue(lineValues[2]);
			}
		} else {
			logger.warn("Could not parse CSV value line: {}", line);
		}
		return part;
	}

}
