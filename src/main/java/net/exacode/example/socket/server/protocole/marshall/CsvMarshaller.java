package net.exacode.example.socket.server.protocole.marshall;

import net.exacode.example.socket.server.protocole.DataFrame;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;

/**
 * Marshalles ServiceData to text (title + CSV).
 * 
 * @author pmendelski
 * 
 */
public class CsvMarshaller implements Marshaller {

	@Override
	public String marshall(DataFrame serviceData) {
		StringBuilder builder = new StringBuilder();
		builder.append(serviceData.getId());
		builder.append(CsvUnmarshaller.LINE_SEPARATOR);
		int valueCount = serviceData.getPartCount();
		if (valueCount > 0) {
			for (int row = 0; row < valueCount; row++) {
				DataFramePart part = serviceData.getPart(row);
				builder.append(part.getType().toString().toLowerCase());
				builder.append(CsvUnmarshaller.VALUE_SEPARATOR);
				builder.append(part.getValueName());
				if (part.getValue() != null) {
					builder.append(CsvUnmarshaller.VALUE_SEPARATOR);
					builder.append(part.getValue());
				}
				builder.append(CsvUnmarshaller.LINE_SEPARATOR);
			}
		}
		// builder.append(CsvUnmarshaller.LINE_SEPARATOR);
		return builder.toString();
	}
}
