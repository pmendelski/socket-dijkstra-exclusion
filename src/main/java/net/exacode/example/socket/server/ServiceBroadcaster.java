package net.exacode.example.socket.server;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.exacode.example.socket.NodeDescriptor;
import net.exacode.example.socket.State;
import net.exacode.example.socket.ValueTypes;
import net.exacode.example.socket.server.protocole.DataFrame;
import net.exacode.example.socket.server.protocole.datapart.DataFramePart;
import net.exacode.example.socket.server.protocole.datapart.DataFramePartType;
import net.exacode.example.socket.server.protocole.marshall.CsvMarshaller;
import net.exacode.example.socket.server.protocole.marshall.CsvUnmarshaller;
import net.exacode.example.socket.server.protocole.marshall.Marshaller;
import net.exacode.example.socket.server.protocole.marshall.Unmarshaller;
import net.exacode.example.socket.timer.Timer;
import net.exacode.example.socket.timer.TimerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible for thread safe broadcasting data.
 * 
 * @author pmendelski
 * 
 */
public class ServiceBroadcaster implements Closeable {

	private final State state;

	private final Map<String, NodeDescriptor> nodeDescriptorMap;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final Marshaller marshaller = new CsvMarshaller();

	private final Unmarshaller unmarshaller = new CsvUnmarshaller();

	private boolean closing = false;

	private boolean connected = false;

	public ServiceBroadcaster(State state) {
		this.state = state;
		nodeDescriptorMap = new HashMap<String, NodeDescriptor>();
		for (NodeDescriptor nodeDescriptor : state.getOtherNodes()) {
			nodeDescriptorMap.put(nodeDescriptor.getId(), nodeDescriptor);
		}
	}

	public void start() throws IOException {
		logger.trace("Connecting with buffer...");
		connectWithNode(state.getBufferNode());
		NodeDescriptor localNode = new NodeDescriptor(state.getBufferNode()
				.getSocket().getLocalAddress().getHostName(),
				state.getLocalServerPort());
		logger.trace("XXXXXXXXXXXXXX Connected with buffer node. Local id: {}",
				localNode.getId());
		state.setNodeDescriptor(localNode);
		logger.debug("Buffer connection established.");
		for (NodeDescriptor node : state.getOtherNodes()) {
			logger.debug("Creating socket for {}:{}", node.getHostAddress(),
					node.getPort());
			boolean connected = false;
			connectWithNode(node);
			logger.debug("Socket created");
		}
		connected = true;
	}

	private NodeDescriptor connectWithNode(NodeDescriptor node) {
		boolean connected = false;
		while (!connected && !closing) {
			try {
				Socket socket = new Socket(node.getHostAddress(),
						node.getPort());
				connected = true;
				node.setSocket(socket);
			} catch (IOException e) {
				logger.warn("Waiting for socket server on {}:{}",
						node.getHostAddress(), node.getPort());
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					logger.error("Could not wait.", e1);
				}
			}
		}
		return node;
	}

	private DataFrame readFrame(NodeDescriptor node) {
		try {
			logger.trace("Waiting for data from: {}", node);
			BufferedReader in = new BufferedReader(new InputStreamReader(node
					.getSocket().getInputStream()));
			StringBuilder input = new StringBuilder();
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.length() == 0) {
					String inputText = input.toString();
					DataFrame data = unmarshaller.unmarshall(inputText);
					if (data != null) {
						logger.trace("Data received:\n{}", inputText);
						return data;
					}
				} else {
					input.append(inputLine);
					input.append("\n");
				}
			}
		} catch (Exception e) {
			logger.error("Error occured while receiving frame.", e);
		}
		return null;
	}

	private void sendFrame(NodeDescriptor node, DataFrame frame) {
		try {
			if (logger.isTraceEnabled()) {
			}
			PrintWriter out = new PrintWriter(node.getSocket()
					.getOutputStream());
			String textFrame = marshaller.marshall(frame);
			logger.trace("Sending frame to {}\n{}", node.getId(), textFrame);
			out.println(marshaller.marshall(frame));
			out.flush();
			logger.trace("Data sent");
		} catch (Exception e) {
			logger.error("Error occured while sending data.", e);
		}
	}

	public boolean isConnected() {
		return connected;
	}

	@Override
	public void close() throws IOException {
		logger.trace("Closing broadcast");
		closing = true;
		List<NodeDescriptor> allNodes = new ArrayList<NodeDescriptor>(
				state.getOtherNodes());
		allNodes.add(state.getBufferNode());
		for (NodeDescriptor nodeDescriptor : allNodes) {
			nodeDescriptor.getSocket().close();
		}
		logger.trace("Broadcast closed");
	}

	public boolean getFirstPhase(String nodeId) throws InterruptedException {
		return Boolean
				.parseBoolean(getVariable(nodeId, ValueTypes.FIRST_PHASE));
	}

	public boolean getSecondPhase(String nodeId) throws InterruptedException {
		return Boolean
				.parseBoolean(getVariable(nodeId, ValueTypes.SECOND_PHASE));
	}

	public int getStock() throws NumberFormatException, InterruptedException {
		return Integer.parseInt(getVariable(state.getBufferNode(),
				ValueTypes.STOCK));
	}

	public String getPriorityNodeId() throws InterruptedException {
		String id = getVariable(state.getBufferNode(), ValueTypes.PRIORITY_NODE);
		if (id.contains("localhost") || id.contains("127.0.0.1")) {
			id = state.getBufferNode().getHostAddress() + ":"
					+ id.split(":")[1];
		}
		return id;
	}

	private String getVariable(String nodeId, ValueTypes valueType)
			throws InterruptedException {
		NodeDescriptor descriptor = nodeDescriptorMap.get(nodeId);
		logger.trace("Mapping id-node: {} - {}", nodeId, descriptor);
		return getVariable(descriptor, valueType);
	}

	public void setStock(int stock) throws InterruptedException {
		setVariable(state.getBufferNode(), ValueTypes.STOCK,
				Integer.toString(stock));
	}

	public void setPriorityNode(String priorityNodeId)
			throws InterruptedException {
		setVariable(state.getBufferNode(), ValueTypes.PRIORITY_NODE,
				priorityNodeId);
	}

	private String getVariable(NodeDescriptor node, ValueTypes valueType)
			throws InterruptedException {
		DataFrame frame = new DataFrame(state.getNodeDescriptor().getId());
		DataFramePart part = new DataFramePart(DataFramePartType.REQ, valueType
				.toString().toLowerCase());
		frame.addPart(part);

		Timer communicationTimer = new TimerImpl();
		communicationTimer.start();
		sendFrame(node, frame);
		DataFrame inputFrame = readFrame(node);
		communicationTimer.stop();
		state.getLocalStatus().addCommunicationTime(
				communicationTimer.getResult());

		DataFramePart inputPart = inputFrame.getPart(0);
		if (!inputPart.getValueName().equalsIgnoreCase(valueType.toString())
				|| !inputPart.getType().equals(DataFramePartType.RESP)) {
			throw new IllegalArgumentException(
					"Wrong frame send as response. Frame: " + frame);
		}
		return inputPart.getValue();
	}

	private void setVariable(NodeDescriptor node, ValueTypes valueType,
			String value) throws InterruptedException {
		DataFrame frame = new DataFrame(state.getNodeDescriptor().getId());
		DataFramePart part = new DataFramePart(DataFramePartType.WRITE,
				valueType.toString().toLowerCase(), value);
		frame.addPart(part);
		sendFrame(node, frame);
	}

	public void sendStatusData() throws InterruptedException {
		Long operationTime = state.getLocalStatus().getOperationAvgTime();
		Long communicationTime = state.getLocalStatus()
				.getCommunicationAvgTime();
		Long processTime = state.getLocalStatus().getProcessAvgTime();
		DataFrame frame = new DataFrame(state.getNodeDescriptor().getId());
		if (communicationTime != null) {
			DataFramePart part = new DataFramePart(DataFramePartType.WRITE,
					ValueTypes.AVG_COMMUNICATION_TIME.toString().toLowerCase(),
					communicationTime.toString());
			frame.addPart(part);
		}
		if (processTime != null) {
			DataFramePart part = new DataFramePart(DataFramePartType.WRITE,
					ValueTypes.AVG_PROCESS_TIME.toString().toLowerCase(),
					processTime.toString());
			frame.addPart(part);
		}
		if (operationTime != null) {
			DataFramePart part = null;
			switch (state.getSystemRole()) {
			case PRODUCER:
				part = new DataFramePart(
						DataFramePartType.WRITE,
						ValueTypes.AVG_PRODUCTION_TIME.toString().toLowerCase(),
						operationTime.toString());
				break;
			case CONSUMER:
				part = new DataFramePart(DataFramePartType.WRITE,
						ValueTypes.AVG_CONSUMPTION_TIME.toString()
								.toLowerCase(), operationTime.toString());
				break;
			default:
				throw new IllegalArgumentException(
						"Status frame must be send only by consumers or producers.");
			}
			frame.addPart(part);
		}
		if (frame.getPartCount() > 0) {
			sendFrame(state.getBufferNode(), frame);
		}
	}
}
