package net.exacode.example.socket.server.protocole.marshall;

import net.exacode.example.socket.server.protocole.DataFrame;

/**
 * Unmarshalls {@link DataFrame} from text.
 * 
 * @author pmendelski
 * 
 */
public interface Unmarshaller {

	DataFrame unmarshall(String text);

}
