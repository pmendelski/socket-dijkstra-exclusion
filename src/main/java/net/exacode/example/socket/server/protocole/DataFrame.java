package net.exacode.example.socket.server.protocole;

import java.util.ArrayList;
import java.util.List;

import net.exacode.example.socket.server.protocole.datapart.DataFramePart;

/**
 * Represents data sent through socket. According to chosen protocol.
 * 
 * 
 * @author pmendelski
 * 
 */
public class DataFrame {

	private final String id;

	private final List<DataFramePart> parts = new ArrayList<DataFramePart>();

	public DataFrame(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public int getPartCount() {
		return parts.size();
	}

	public DataFramePart getPart(int i) {
		return parts.get(i);
	}

	public void addPart(DataFramePart part) {
		if (part != null) {
			parts.add(part);
		}
	}

}
