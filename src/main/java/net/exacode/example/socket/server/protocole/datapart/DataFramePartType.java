package net.exacode.example.socket.server.protocole.datapart;

/**
 * Represents a type of {@link DataFramePart}
 * 
 * @author pmendelski
 * 
 */
public enum DataFramePartType {
	/**
	 * Request for a value of some variable.
	 */
	REQ,
	/**
	 * Response with a value of some variable.
	 */
	RESP,
	/**
	 * Request to change value of some varaible.
	 */
	WRITE
}
