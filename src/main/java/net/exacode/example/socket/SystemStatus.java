package net.exacode.example.socket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemStatus {
	private final Map<String, NodeStatus> nodeStatuses = new HashMap<String, NodeStatus>();

	public NodeStatus getNodeStatus(String nodeId) {
		NodeStatus nodeStatus = nodeStatuses.get(nodeId);
		if (nodeStatus == null) {
			synchronized (nodeStatuses) {
				nodeStatus = new NodeStatus();
				nodeStatus.setNodeId(nodeId);
				nodeStatuses.put(nodeId, nodeStatus);
			}
		}
		return nodeStatus;
	}

	public List<NodeStatus> getNodeStatuses() {
		return new ArrayList<NodeStatus>(nodeStatuses.values());
	}

}
