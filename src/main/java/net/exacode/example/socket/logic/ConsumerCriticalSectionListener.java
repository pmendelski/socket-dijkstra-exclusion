package net.exacode.example.socket.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Consumer action performed on critical section access.
 * 
 * @author pmendelski
 * 
 */
public class ConsumerCriticalSectionListener implements CriticalSectionListener {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public int criticalSection(int stock) {
		if (stock == 0) {
			logger.debug(
					"Could not consume stock unit. Stock is empty. Stock: {}",
					stock);
			return stock;
		}
		int newStock = stock - 1;
		logger.debug("Changing stock from {} to {}", stock, newStock);
		return newStock;
	}

	@Override
	public boolean readyToEnterFirstPhase(int stock) {
		return stock > 0;
	}

}
