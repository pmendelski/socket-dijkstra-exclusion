package net.exacode.example.socket.logic;

/**
 * Responsible for performing actions in critical section.
 * 
 * @author pmendelski
 * 
 */
public interface CriticalSectionListener {
	int criticalSection(int stock);

	boolean readyToEnterFirstPhase(int stock);
}
