package net.exacode.example.socket.logic;

import net.exacode.example.socket.NodeDescriptor;
import net.exacode.example.socket.State;
import net.exacode.example.socket.server.ServiceBroadcaster;
import net.exacode.example.socket.timer.Timer;
import net.exacode.example.socket.timer.TimerImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Responsible for imeplementing specific steps of
 * {@link DijkstraExclusionTemplate}.
 * 
 * @author pmendelski
 * 
 */
public class SocketDijkstraExclusionTemplate extends DijkstraExclusionTemplate {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final State state;

	private final ServiceBroadcaster broadcaster;

	private final String[] otherNodeIds;

	private final CriticalSectionListener criticalSectionListener;

	private Timer timer;

	public SocketDijkstraExclusionTemplate(final State state,
			ServiceBroadcaster broadcaster,
			CriticalSectionListener criticalSectionListener) {
		super();
		this.state = state;
		this.broadcaster = broadcaster;
		this.criticalSectionListener = criticalSectionListener;
		otherNodeIds = new String[state.getOtherNodes().size()];
		int i = 0;
		for (NodeDescriptor descriptor : state.getOtherNodes()) {
			otherNodeIds[i++] = descriptor.getId();
		}
	}

	@Override
	public void start() throws NumberFormatException, InterruptedException {
		timer = new TimerImpl();
		timer.start();
		super.start();
	}

	@Override
	protected String getId() throws InterruptedException {
		return state.getNodeDescriptor().getId();
	}

	@Override
	protected String[] getOtherNodeIds() {
		return otherNodeIds;
	}

	@Override
	protected void setFirstPhase(boolean firstPhase) {
		state.setFirstPhase(firstPhase);
	}

	@Override
	protected void setSecondPhase(boolean secondPhase) {
		state.setSecondPhase(secondPhase);
	}

	@Override
	protected boolean isFirstPhase() {
		return state.isFirstPhase();
	}

	@Override
	protected boolean isSecondPhase() {
		return state.isSecondPhase();
	}

	@Override
	protected boolean isFirstPhase(String id) throws InterruptedException {
		return broadcaster.getFirstPhase(id);
	}

	@Override
	protected boolean isSecondPhase(String id) throws InterruptedException {
		return broadcaster.getSecondPhase(id);
	}

	@Override
	protected String getPriorityId() throws InterruptedException {
		return broadcaster.getPriorityNodeId();
	}

	@Override
	protected void setPriorityId(String id) throws InterruptedException {
		broadcaster.setPriorityNode(id);
	}

	@Override
	protected int getStock() throws NumberFormatException, InterruptedException {
		return broadcaster.getStock();
	}

	@Override
	protected void setStock(int stock) throws InterruptedException {
		broadcaster.setStock(stock);
	}

	@Override
	protected int criticalSection(int stock) {
		logger.trace("<<<<<<<<<<<< Operation timer STOP");
		timer.stop();
		state.getLocalStatus().addOperationTime(timer.getResult());
		return criticalSectionListener.criticalSection(stock);
	}

	@Override
	protected void afterCriticalSection() throws InterruptedException {
		Thread.sleep(500);
		while (!criticalSectionListener.readyToEnterFirstPhase(getStock())) {
			Thread.sleep(1000);
		}
		logger.trace(">>>>>>>>>>> Operation timer START");
		timer.start();
	}

}
