package net.exacode.example.socket.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TemplateMethod pattern object used to implement Dijkstra exclusion algorithm.
 * 
 * @author pmendelski
 * 
 */
public abstract class DijkstraExclusionTemplate {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void start() throws NumberFormatException, InterruptedException {
		setPriorityId(getId());
		while (true) {
			setFirstPhase(true);
			while (!isSecondPhase()) {
				// Entry phase 1
				logger.trace("In first phase");
				while (!getId().equals(getPriorityId())) {
					if (!isFirstPhase(getPriorityId())) {
						setPriorityId(getId());
					}
				}
				// Entry phase 2
				logger.trace("In second phase");
				setSecondPhase(true);
				for (String id : getOtherNodeIds()) {
					if (isSecondPhase(id)) {
						setSecondPhase(false);
						break;
					}
				}
			}
			// Critical section
			logger.trace("In critical section");
			int oldStock = getStock();
			int newStock = criticalSection(oldStock);
			if (newStock != oldStock) {
				setStock(newStock);
			}
			// Algorithm reset
			setSecondPhase(false);
			setFirstPhase(false);
			afterCriticalSection();
		}
	}

	protected abstract void afterCriticalSection() throws InterruptedException;

	protected abstract String getId() throws InterruptedException;

	protected abstract String[] getOtherNodeIds();

	protected abstract void setFirstPhase(boolean firstPhase);

	protected abstract void setSecondPhase(boolean secondPhase);

	protected abstract boolean isFirstPhase();

	protected abstract boolean isSecondPhase();

	protected abstract boolean isFirstPhase(String id)
			throws InterruptedException;

	protected abstract boolean isSecondPhase(String id)
			throws InterruptedException;

	protected abstract String getPriorityId() throws InterruptedException;

	protected abstract void setPriorityId(String id)
			throws InterruptedException;

	protected abstract int getStock() throws NumberFormatException,
			InterruptedException;

	protected abstract void setStock(int stock) throws InterruptedException;

	protected abstract int criticalSection(int stock);
}
