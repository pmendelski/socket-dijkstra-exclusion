package net.exacode.example.socket.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Producer action performed on critical section access.
 * 
 * @author pmendelski
 * 
 */
public class ProducerCriticalSectionListener implements CriticalSectionListener {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public int criticalSection(int stock) {
		int newStock = stock + 1;
		logger.debug("Changing stock from {} to {}", stock, newStock);
		return newStock;
	}

	@Override
	public boolean readyToEnterFirstPhase(int stock) {
		return stock < Integer.MAX_VALUE;
	}

}
