#!/bin/bash
LOCALHOST="127.0.0.1"
DIAGNOSTIC_DELTA=100000
JAR_FILE="socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar"

echo "Buffer address: $1"

BUFFER=$1

cd ../../../../target

# Start terminal
echo "Starting buffer..."
PORT=${BUFFER#*:}
java -jar socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar -r BUFFER -p $PORT -diagnosic $DIAGNOSTIC_DELTA &

