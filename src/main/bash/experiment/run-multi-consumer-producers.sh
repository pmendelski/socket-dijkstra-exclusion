#!/bin/bash
LOCALHOST="127.0.0.1"
DIAGNOSTIC_DELTA=100000
JAR_FILE="socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar"

CONSUMER_BASE_PORT=8100;
PRODUCER_BASE_PORT=8300;

echo "Producers: $1"
echo "Consumers: $2"
echo "Buffer address: $3"

BUFFER=$3
for IDX in $(seq 0 1 $1)
do
	PORT=$(($PRODUCER_BASE_PORT + $IDX))
	PRODUCERS[$IDX]="$LOCALHOST:$PORT"
done

for IDX in $(seq 0 1 $2)
do
	PORT=$(($CONSUMER_BASE_PORT + $IDX))
	CONSUMERS[$IDX]="$LOCALHOST:$PORT"
done

NODES_CSV=$(printf ",%s" "${CONSUMERS[@]}")$(printf ",%s" "${PRODUCERS[@]}")
NODES_CSV=${NODES_CSV:1}
echo "All nodes: $NODES_CSV"

cd ../../../../target

# Start consumers
echo "Starting consumers..."
for ADDRESS in "${CONSUMERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting consumer on port: $PORT"
	java -jar $JAR_FILE -r CONSUMER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA &
done

# Start producers
echo "Starting producers..."
for ADDRESS in "${PRODUCERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting producer on port: $PORT"
	java -jar $JAR_FILE -r PRODUCER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA &
done
