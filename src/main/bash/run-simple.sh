#!/bin/bash
LOCALHOST="127.0.0.1"
DIAGNOSTIC_DELTA=100000
SLOW_RESPONSE=0 #1000
JAR_FILE="socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar"
#LOG_CONFIG_FILE="../src/main/bash/logback-info.xml"
LOG_CONFIG_FILE="../src/main/bash/logback-debug.xml"
#LOG_CONFIG_FILE="../src/main/bash/logback-trace.xml"


BUFFER="$LOCALHOST:8088"
CONSUMERS[0]="$LOCALHOST:8090"
PRODUCERS[0]="$LOCALHOST:8100"

NODES_CSV=$(printf ",%s" "${CONSUMERS[@]}")$(printf ",%s" "${PRODUCERS[@]}")
NODES_CSV=${NODES_CSV:1}
echo "All nodes: $NODES_CSV"

cd ../../../target

# Start consumers
echo "Starting consumers..."
for ADDRESS in "${CONSUMERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting consumer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r CONSUMER -p $PORT -b $BUFFER -cp $OTHER_NODES -slow $SLOW_RESPONSE -log $LOG_CONFIG_FILE -diagnosic $DIAGNOSTIC_DELTA"
done

# Start producers
echo "Starting producers..."
for ADDRESS in "${PRODUCERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting producer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r PRODUCER -p $PORT -b $BUFFER -cp $OTHER_NODES -slow $SLOW_RESPONSE -log $LOG_CONFIG_FILE -diagnosic $DIAGNOSTIC_DELTA"
done

# Start terminal
echo "Starting buffer..."
PORT=${BUFFER#*:}
#gnome-terminal -e "java -jar socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar -r BUFFER -p $PORT -slow $SLOW_RESPONSE -log $LOG_CONFIG_FILE -diagnosic $DIAGNOSTIC_DELTA"
java -jar socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar -r BUFFER -p $PORT -slow $SLOW_RESPONSE -log $LOG_CONFIG_FILE -diagnosic $DIAGNOSTIC_DELTA
