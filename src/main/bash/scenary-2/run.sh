#!/bin/bash
BUFFER_ADDRESS="127.0.0.1:8080"
CONSUMER_COUNT=1
PRODUCER_COUNT=1

EXTERNAL_BASE_PORT=9000
EXTERNAL_NODE_COUNT=2
EXTERNAL_HOST=192.168.43.194


PORT=$EXTERNAL_BASE_PORT
HOST=$EXTERNAL_HOST
for IDX in $(seq 1 $EXTERNAL_NODE_COUNT)
do
	PORT=$(($PORT + 1))
	NODES_CSV="$NODES_CSV,$HOST:$PORT"
done

NODES_CSV=${NODES_CSV:1}
echo "xternal nodes: $NODES_CSV"

./run-multi-consumer-producers.sh $PRODUCER_COUNT $CONSUMER_COUNT $BUFFER_ADDRESS $NODES_CSV
./run-buffer.sh $BUFFER_ADDRESS
