#!/bin/bash
LOCALHOST="127.0.0.1"
DIAGNOSTIC_DELTA=10000
JAR_FILE="socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar"
LOG_CONFIG_FILE="../src/main/bash/logback-debug.xml"

PORT=8100;

echo "Producers: $1"
echo "Consumers: $2"
echo "Buffer address: $3"
echo "External addresses: $4"

BUFFER=$3
for IDX in $(seq 1 1 $1)
do
	PORT=$(($PORT + 1))
	PRODUCERS[$IDX]="$LOCALHOST:$PORT"
	NODES_CSV="$NODES_CSV,$LOCALHOST:$PORT"
done

for IDX in $(seq 1 1 $2)
do
	PORT=$(($PORT + 1))
	CONSUMERS[$IDX]="$LOCALHOST:$PORT"
	NODES_CSV="$NODES_CSV,$LOCALHOST:$PORT"
done

NODES_CSV=${NODES_CSV:1},$4
echo "All nodes: $NODES_CSV"

cd ../../../../target

# Start consumers
echo "Starting consumers..."
for ADDRESS in "${CONSUMERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting consumer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r CONSUMER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA"
done

# Start producers
echo "Starting producers..."
for ADDRESS in "${PRODUCERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting producer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r PRODUCER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA"
done
