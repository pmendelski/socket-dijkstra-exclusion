#!/bin/bash
LOCALHOST="127.0.0.1"
DIAGNOSTIC_DELTA=1000
JAR_FILE="socket-dijkstra-exclusion-0.0.1-SNAPSHOT.jar"
LOG_CONFIG_FILE="../src/main/bash/logback-debug.xml"

CONSUMER_BASE_PORT=8100;
PRODUCER_BASE_PORT=8300;

echo "Producers: $1"
echo "Consumers: $2"
echo "Buffer address: $3"
echo "External addresses: $4"

BUFFER=$3
for IDX in $(seq 1 1 $1)
do
	PORT=$(($PRODUCER_BASE_PORT + $IDX))
	PRODUCERS[$IDX]="$LOCALHOST:$PORT"
done

for IDX in $(seq 1 1 $2)
do
	PORT=$(($CONSUMER_BASE_PORT + $IDX))
	CONSUMERS[$IDX]="$LOCALHOST:$PORT"
done

NODES_CSV=$(printf ",%s" "${CONSUMERS[@]}")$(printf ",%s" "${PRODUCERS[@]}"),$4
NODES_CSV=${NODES_CSV:1}
echo "All nodes: $NODES_CSV"

cd ../../../../target

# Start consumers
echo "Starting consumers..."
for ADDRESS in "${CONSUMERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting consumer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r CONSUMER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA -log $LOG_CONFIG_FILE"
done

# Start producers
echo "Starting producers..."
for ADDRESS in "${PRODUCERS[@]}"
do
	OTHER_NODES=${NODES_CSV/"$ADDRESS,"/}
	OTHER_NODES=${OTHER_NODES/",$ADDRESS"/}
	PORT=${ADDRESS#*:}
	echo "Starting producer on port: $PORT"
	gnome-terminal -e "java -jar $JAR_FILE -r PRODUCER -p $PORT -b $BUFFER -cp $OTHER_NODES -diagnosic $DIAGNOSTIC_DELTA -log $LOG_CONFIG_FILE"
done
